"""Package for WikipediaTextOnly cog."""
from .wikipediato import WikipediaTextOnly

__red_end_user_data_statement__ = (
    "This cog does not persistently store data or metadata about users."
)


def setup(bot):
    """Load WikipediaTextOnly cog."""
    cog = WikipediaTextOnly()
    bot.add_cog(cog)
