"""Unity's custom cog for Red-DiscordBot by desentizised."""
import discord
from redbot.core import __version__ as redbot_version
from redbot.core import commands
from redbot.core.utils.chat_formatting import error, warning

__author__ = "desentizised"
__icon__ = "https://cdn.discordapp.com/avatars/765271044234936402/cba245bc2985a19ebcb9fb8d5a28cef9.png?size=32"
__footer__ = "Of course you can always use `u!help` to get to the full list of commands via DM."


class Unity(commands.Cog):
    """Unity's own personal Cog."""

    def __init__(self, bot):
        self.bot = bot

    async def red_delete_data_for_user(self, **kwargs):
        """Nothing to delete."""
        return

    @commands.command(aliases=["ahelp"])
    async def abbrhelp(self, ctx):
        """Get an abbreviated overview of this bot's functions."""
        
        try:
            description = """`u!help Unity` <<< **Unity's own commands**
            `u!aliaslist` some fun aliases mostly for creating memes
            Research Stuff:
            `u!conv <c/f|km/mi|kg/lb> <c/f|km/mi|kg/lb> <value>` conversions
            `u!define <term>` dictionary
            `u!googlesay <lang id> <text>` Google TTS
            `u!synonym <term>` thesaurus
            `u!wiki <term>` query wikipedia
            `u!wolfram(image/solve) <query>` **query wolfram alpha**
            Text Stuff:
            `u!8 <question>` ask magic 8-ball
            `u!react <reaction> <message id>` react with text, message id optional
            `u!shout <text>` SHOUT IN :regional_indicator_b:​:regional_indicator_i:​:regional_indicator_g: LETTERS
            `u!space <space count> <text>` write text with spaces inbetween
            Meta Stuff:
            `u!bigmoji <emote>`
            `u!defcon(+/-)`
            `u!insult <user>` insult someone
            `u!mock <message id> or <text>` mock what someone said
            `u!nuke/deepfry <image url>` alter an image
            Media Stuff:
            `u!gif <query>` the first GIPHY result
            `u!gifr <query>` a random GIPHY result
            `u!imgs <query>` query imgur
            `u!movie <title>` info about movies
            `u!tv <title>` info about TV shows"""
    
            embed = discord.Embed(
                #title="Quick Cheat Sheet:",
                description=u"\u2063\n{}\n\u2063".format(description),
                color=discord.Color.blue(),
            )
            embed.set_author(
                name="Unity Abbreviated Help Menu", 
                icon_url=__icon__
            )
            embed.set_footer(
                text=__footer__
            )
            await ctx.send(embed=embed)
        
        except:
            await ctx.send("Something went wrong.")

    @commands.command(aliases=["chelp"])
    async def coghelp(self, ctx):
        """Get pointers to some interesting cogs."""
        
        try:
            description = """Meme Stuff:
            `u!aliaslist`
            `u!help Comics` list available comic commands
            `u!help Fun` what the name implies
            `u!help ImageMaker` image generation
            `u!help NotSoBot` more image generation
            `u!help Unity`
            Research Stuff:
            `u!help Conversions` conversions
            `u!help Wolfram` all wolfram alpha commands"""
    
            embed = discord.Embed(
                #title="Quick Cheat Sheet:",
                description=u"\u2063\n{}\n\u2063".format(description),
                color=discord.Color.blue(),
            )
            embed.set_author(
                name="Unity Installed Cogs Help Menu", 
                icon_url=__icon__
            )
            embed.set_footer(
                text=__footer__
            )
            await ctx.send(embed=embed)
        
        except:
            await ctx.send("Something went wrong.")

    @commands.command()
    async def actually(self, ctx):
        """if you think about it"""
        await ctx.send("https://tenor.com/view/actually-bored-ackchyually-gif-14423655")

    @commands.command()
    async def agile(self, ctx):
        """af"""
        await ctx.send("https://tenor.com/view/gymnastic-flexible-acrobatic-athletic-sporty-gif-12684370")

    @commands.command()
    async def almost(self, ctx):
        """holy shit bro"""
        await ctx.send("https://cdn.discordapp.com/attachments/724055819548622968/821127474510692442/qwertyuioiujhgfghjkljhgfhjkjhgfghjhgf.jpeg")

    @commands.command(aliases=["clap"])
    async def applause(self, ctx):
        """by bud"""
        await ctx.send("https://giphy.com/gifs/bud-dD26OUpjq8vdu")

    @commands.command()
    async def aww(self, ctx):
        """so cute"""
        await ctx.send("https://giphy.com/gifs/awww-26hpKMTa5HgdSMhRC")

    @commands.command()
    async def badumtss(self, ctx):
        """very funny"""
        await ctx.send("https://giphy.com/gifs/ben-tattoo-giant-cD7PLGE1KWOhG")

    @commands.command()
    async def bigmac(self, ctx):
        """be floppin"""
        await ctx.send("https://imgur.com/ehgy01x")

    @commands.command()
    async def bonk(self, ctx):
        """go to horny jail"""
        await ctx.send("https://tenor.com/view/horny-lais-evil-laughter-gif-19294476")

    @commands.command()
    async def brexit(self, ctx):
        """be like this"""
        await ctx.send("https://tenor.com/view/brexit-gif-5902235")

    @commands.command()
    async def congratulations(self, ctx):
        """to you"""
        await ctx.send("https://cdn.discordapp.com/attachments/395558278903496715/783688315731443712/Congratulations.gif")
        await ctx.send("<https://www.youtube.com/watch?v=1Bix44C1EzY>")

    @commands.command()
    async def dasnixgut(self, ctx):
        """wallah"""
        await ctx.send("https://static.mydealz.de/live/comments/content/tkOmz/20388958.jpg")

    @commands.command()
    async def didntread(self, ctx):
        """LUL"""
        await ctx.send("https://tenor.com/view/didnt-read-gif-5176665")

    @commands.command()
    async def dundundun(self, ctx):
        """major plot twist"""
        await ctx.send("https://tenor.com/view/dundundun-suspense-scary-shocker-gif-6070360")

    @commands.command()
    async def egal(self, ctx):
        """I know it anyways"""
        await ctx.send("https://tenor.com/view/egal-singing-ocean-sea-boat-ride-gif-16080257")

    @commands.command()
    async def enough(self, ctx):
        """is enough"""
        await ctx.send("https://cdn.discordapp.com/attachments/415389551952134145/776598007206510602/124622503_10159209338163881_765143382160427917_n.png")

    @commands.command()
    async def fickteuch(self, ctx):
        """allee"""
        await ctx.send("https://cdn.discordapp.com/attachments/321423915492835341/782965317969117214/zf33u44owc261.png")

    @commands.command()
    async def gays4donald(self, ctx):
        """drill my ass with the strap-on"""
        await ctx.send("https://open.spotify.com/track/6lH7E41rJRKTkMzo5p4uJB")

    @commands.command()
    async def goodbye(self, ctx):
        """Thomas Meme"""
        await ctx.send("https://tenor.com/view/thomas-it-was-time-for-thomas-to-leave-he-had-seen-everything-it-was-time-for-thomas-to-leave-he-had-seen-everything-gif-19039945")

    @commands.command()
    async def hackerman(self, ctx):
        """he be hackin'"""
        await ctx.send("https://tenor.com/view/hacker-hackerman-kung-fury-gif-7953536")

    @commands.command()
    async def haltstop(self, ctx):
        """jetzt rede ich"""
        await ctx.send("https://tenor.com/view/stop-angry-annoyed-frauentausch-halt-stop-gif-17066006")

    @commands.command(aliases=["health"])
    async def helth(self, ctx):
        """stonks"""
        await ctx.send("https://cdn.discordapp.com/attachments/687754336180502567/797486095276572672/f1e.png")

    @commands.command()
    async def hibye(self, ctx):
        """was just checkin'"""
        await ctx.send("https://tenor.com/view/simpsons-bart-simpson-grampa-simpson-old-man-hi-bye-gif-8390063")

    @commands.command()
    async def hmm(self, ctx):
        """buzz is not sure"""
        await ctx.send("https://cdn.discordapp.com/attachments/752894651610038403/792037172089061446/unknown.png")

    @commands.command()
    async def hmmm(self, ctx):
        """?"""
        await ctx.send("https://tenor.com/view/hmmm-think-emoji-flex-thinking-gif-17437718")

    @commands.command()
    async def icanteven(self, ctx):
        """or odd"""
        await ctx.send("https://tenor.com/view/funny-drunk-beer-smashed-gif-15671630")

    @commands.command()
    async def idontlikeit(self, ctx):
        """that's going on"""
        await ctx.send("https://cdn.discordapp.com/attachments/321423915492835341/807565614410891284/cover6.png")

    @commands.command()
    async def ifyoudothething(self, ctx):
        """and you do it right"""
        await ctx.send("https://cdn.discordapp.com/attachments/765279162457129032/797303323660386304/Jontron_-_If_you_do_the_thing-2.gif")
        await ctx.send("<https://youtu.be/8ci2hj7CSHI?t=574>")

    @commands.command()
    async def jediseal(self, ctx):
        """copypasta"""
        await ctx.send("What da doo-doo did yousa just say about meesa, yousa litta bitty bitch? I’ll has yousa know meesa graduated bombad of meesa class in da Jedi Seals, and meesa been involved in numerous secret raids on da Federation, and meesa has over 300 confirmed kills. Meesa trained in guerilla warfare and meesa da bombad sniper in da entire Republic armed forces. Yousa nutting to meesa but just another target. Meesa ganna wipe yousa da doo-doo out with precision da likein of which has never been seen before on dis Naboo, mark meesa doo-doo words. Yousa thinkin yousa ganna getin away with sayin da doo-doo to meesa over da internet? Thinkin again, fucker. As weesa speak meesa contacting meesa secret network of spies across da Republic and yousa IP is bein traced right now so yousa bombad prepare for da storm, maggot. Da storm dat wipes out da pathetic litta bitty thing yousa call yousa life. Yousa doo-doo dead, kid. Meesa ganna be anywhere, anytime, and meesa ganna kill yousa in over seven hundred ways, and that’s just with meesa bare hands. Not only meesa extensive train in unarm combat, but meesa access to da entire arsenal of da Republic Jedi Order and meesa ganna use it to its full extent to wipe yousa miserable doo-doo off da face of da galaxy, yousa litta bitty doo-doo. If only yousa can has known what unholy retribution yousa litta bitty “clever” comment was about to bring down upon yousa, maybe yousa woulda held yousa doo-doo tongue. But yousa couldn’t, yousa didn’t, and now yousa gonna pay da price, yousa goddamn idiot. Meesa ganna doo-doo fury all over yousa and yousa ganna drown in it. Yousa doo-doo dead, kiddo.")

    @commands.command()
    async def kirby(self, ctx):
        """being kirby"""
        await ctx.send("https://tenor.com/view/hungry-pokemon-kirby-gif-3566133")

    @commands.command()
    async def konrad(self, ctx):
        """verachtet dich"""
        await ctx.send("https://cdn.discordapp.com/attachments/431247358164992000/836551972584685598/EaVrY_0XsAE1ojp.png")

    @commands.command()
    async def letmein(self, ctx):
        """!!!!!1!!oneeleven"""
        await ctx.send("https://giphy.com/gifs/yx400dIdkwWdsCgWYp")

    @commands.command()
    async def meyou(self, ctx):
        """and miller's cow"""
        await ctx.send("https://tenor.com/view/dragon-car-gif-19597879")

    @commands.command()
    async def mindblown(self, ctx):
        """the one and only"""
        await ctx.send("https://media.giphy.com/media/xT0xeJpnrWC4XWblEk/giphy.gif")

    @commands.command()
    async def modwalk(self, ctx):
        """copypasta"""
        await ctx.send("-( ͡° ͜ʖ ͡°)╯╲___卐卐卐卐 Don't mind me just taking my mods for a walk")

    @commands.command()
    async def navyseal(self, ctx):
        """the pasta among pastas"""
        await ctx.send("What the fuck did you just fucking say about me, you little bitch? I'll have you know I graduated top of my class in the Navy Seals, and I've been involved in numerous secret raids on Al-Quaeda, and I have over 300 confirmed kills. I am trained in gorilla warfare and I'm the top sniper in the entire US armed forces. You are nothing to me but just another target. I will wipe you the fuck out with precision the likes of which has never been seen before on this Earth, mark my fucking words. You think you can get away with saying that shit to me over the Internet? Think again, fucker. As we speak I am contacting my secret network of spies across the USA and your IP is being traced right now so you better prepare for the storm, maggot. The storm that wipes out the pathetic little thing you call your life. You're fucking dead, kid. I can be anywhere, anytime, and I can kill you in over seven hundred ways, and that's just with my bare hands. Not only am I extensively trained in unarmed combat, but I have access to the entire arsenal of the United States Marine Corps and I will use it to its full extent to wipe your miserable ass off the face of the continent, you little shit. If only you could have known what unholy retribution your little “clever” comment was about to bring down upon you, maybe you would have held your fucking tongue. But you couldn't, you didn't, and now you're paying the price, you goddamn idiot. I will shit fury all over you and you will drown in it. You're fucking dead, kiddo.")

    @commands.command()
    async def nein(self, ctx):
        """oder doch?"""
        await ctx.send("https://cdn.discordapp.com/attachments/329607887275950081/760199052028936252/ezgif-2-1343befd85d4.gif")

    @commands.command()
    async def nice(self, ctx):
        """signs the young boy"""
        await ctx.send("https://tenor.com/view/thumbsup-cool-great-nice-notbad-gif-5220607")

    @commands.command()
    async def no(self, ctx):
        """Rage Face"""
        await ctx.send("https://i.kym-cdn.com/entries/icons/original/000/007/423/RageFace.jpg")

    @commands.command()
    async def nomnom(self, ctx):
        """goes the doge"""
        await ctx.send("https://media.discordapp.net/attachments/491967870964596756/721641189194989608/image0.gif")
        await ctx.send("https://media.discordapp.net/attachments/491967870964596756/721641189400641536/image1.gif")

    @commands.command()
    async def nottoday(self, ctx):
        """what do we say?"""
        await ctx.send("https://giphy.com/gifs/game-of-thrones-got-arya-stark-9RKLlD2oz5c7m")

    @commands.command(aliases=["ok"])
    async def okay(self, ctx):
        """then"""
        await ctx.send("https://giphy.com/gifs/pz7X6ivsb8KaEO9D4F")

    @commands.command()
    async def omfg(self, ctx):
        """holy wow"""
        await ctx.send("https://giphy.com/gifs/owl-surprised-EdRgVzb2X3iJW")

    @commands.command()
    async def orly(self, ctx):
        """?"""
        await ctx.send("https://giphy.com/gifs/zach-galifianakis-oh-really-between-two-ferns-3oEjI3pZn3hFRNtzSo")

    @commands.command()
    async def owned(self, ctx):
        """!!!!!!!!!!!!"""
        await ctx.send("https://tenor.com/view/bruh-swag-savage-burn-owned-gif-5251169")
        
    @commands.command()
    async def paddlin(self, ctx):
        """is what that is"""
        await ctx.send("https://tenor.com/view/paddling-gif-7623907")
    
    @commands.command()
    async def parkour(self, ctx):
        """Office Meme"""
        await ctx.send("https://tenor.com/view/parkour-gif-9979576")

    @commands.command()
    async def pleaseevacuate(self, ctx):
        """self-destruction imminent"""
        await ctx.send("https://tenor.com/view/self-destruction-imminent-please-evacuate-gif-8912211")

    @commands.command()
    async def pouroneout(self, ctx):
        """for teh bois"""
        await ctx.send("https://giphy.com/gifs/abcnetwork-blackish-AHwp4ONNkDumc")

    @commands.command()
    async def qapla(self, ctx):
        """Star Trek Meme"""
        await ctx.send("https://tenor.com/view/qapla-quark-klingon-qapla-to-you-too-gif-17352789")

    @commands.command()
    async def reaction(self, ctx):
        """image"""
        await ctx.send("https://cdn.discordapp.com/attachments/708056401720639509/749022116346724373/IMG_20200402_221755.jpg")

    @commands.command()
    async def risitas(self, ctx):
        """much lol, very lmao"""
        await ctx.send("https://tenor.com/view/lol-risa-risitas-laught-jaja-gif-14980369")

    @commands.command()
    async def sendhug(self, ctx):
        """virtually"""
        await ctx.send("https://tenor.com/view/sending-virtual-hug-virtual-hug-hug-gif-3550852")
    
    @commands.command()
    async def shitpost(self, ctx):
        """is what this is"""
        await ctx.send("https://imgur.com/uR3Ptqf")

    @commands.command()
    async def squirtsquirt(self, ctx):
        """as in ejaculation"""
        await ctx.send("https://tenor.com/view/horny-nsfw-gif-14371044")

    @commands.command()
    async def steering(self, ctx):
        """a racecar"""
        await ctx.send("https://streamable.com/nlzno4")

    @commands.command()
    async def swiggity(self, ctx):
        """swooty"""
        await ctx.send("https://media.giphy.com/media/Iupjt7NLfJsty/giphy.gif")

    @commands.command()
    async def thatsillegal(self, ctx):
        """Halo Meme"""
        await ctx.send("https://tenor.com/view/wait-thats-illegal-halo-meme-gif-14048618")

    @commands.command()
    async def themissile(self, ctx):
        """copypasta"""
        await ctx.send("The missile knows where it is at all times. It knows this because it knows where it isn't, by subtracting where it is, from where it isn't, or where it isn't, from where it is - whichever is greater - it obtains a difference, or deviation. The guidance sub-system uses deviations to generate corrective commands to drive the missile from a position where it is, to a position where it isn't, and arriving at a position where it wasn't, it now is. Consequently, the position where it is, is now the position that it wasn't, and it follows that the position that it was, is now the position that it isn't. In the event of the position that it is in is not the position that it wasn't, the system has acquired a variation. The variation being the difference between where the missile is, and where it wasn't. If variation is considered to be a significant factor, it too, may be corrected by the GEA. However, the missile must also know where it was. The missile guidance computer scenario works as follows: Because a variation has modified some of the information the missile has obtained, it is not sure just where it is, however it is sure where it isn't, within reason, and it knows where it was. It now subtracts where it should be, from where it wasn't, or vice versa. And by differentiating this from the algebraic sum of where it shouldn't be, and where it was. It is able to obtain the deviation, and its variation, which is called “air”.")
        await ctx.send("<https://www.youtube.com/watch?v=bZe5J8SVCYQ>")

    @commands.command()
    async def thisisfine(self, ctx):
        """totally fine"""
        await ctx.send("https://tenor.com/view/this-is-fine-fire-house-burning-okay-gif-5263684")

    @commands.command()
    async def toobad(self, ctx):
        """nice try"""
        await ctx.send("https://i.redd.it/7tjsrw5q3kk61.jpg")

    @commands.command()
    async def twitterbelike(self, ctx):
        """Selfmade Meme"""
        await ctx.send("https://cdn.discordapp.com/attachments/765279162457129032/788223991357374484/Screenshot_2020-12-12_at_12.55.47.png")

    @commands.command()
    async def unity(self, ctx):
        """The ubiquitous one"""
        await ctx.send("https://www.youtube.com/watch?v=ZNs3aJ8BwZY")

    @commands.command()
    async def usefulmeme(self, ctx):
        """TYVM"""
        await ctx.send("https://media.discordapp.net/attachments/680928395399266314/781267033882230815/unknown.png")

    @commands.command(aliases=["w0tm8"])
    async def uwot(self, ctx):
        """m8"""
        await ctx.send("https://tenor.com/view/u-wot-m8-cheeky-burger-rekt-rek-ill-rek-you-gif-5399029")
    
    @commands.command()
    async def wasted(self, ctx):
        """Teletubby"""
        await ctx.send("https://tenor.com/view/teletubbies-lala-wasted-killed-dies-gif-16750151")

    @commands.command()
    async def wat(self, ctx):
        """eh?"""
        await ctx.send("https://tenor.com/view/what-wat-wat-lady-confused-huh-gif-8314795")

    @commands.command()
    async def what(self, ctx):
        """!?!"""
        await ctx.send("https://tenor.com/view/brooklyn-nine-nine-andy-samberg-jake-peralta-what-happy-gif-5152300")

    @commands.command()
    async def whoops(self, ctx):
        """that's unfortunate"""
        await ctx.send("https://tenor.com/view/carwash-infomercial-gif-10220195")
        
    @commands.command()
    async def whopingedme(self, ctx):
        """who dafuq pinged me?!"""
        await ctx.send("https://tenor.com/view/annoying-who-pinged-me-angry-gif-14512411")

    @commands.command()
    async def whyamihere(self, ctx):
        """don't ask don't tell"""
        await ctx.send("https://cdn.discordapp.com/attachments/321423915492835341/770681969981521950/image0-13-1.png")

    @commands.command()
    async def yay(self, ctx):
        """Carlton Meme"""
        await ctx.send("https://tenor.com/view/excited-dance-yay-happy-dance-gif-12553863")

    @commands.command()
    async def yes(self, ctx):
        """uh huh"""
        await ctx.send("https://tenor.com/view/ron-pearlman-the-goon-yes-yep-anchorman-gif-12449331")

    @commands.command()
    async def yike(self, ctx):
        """award"""
        await ctx.send("https://tenor.com/view/yikes-big-yikes-oops-award-awkward-gif-14697236")
        
    @commands.command()
    async def yourewinner(self, ctx):
        """always"""
        await ctx.send("https://tenor.com/view/youre-winner-winner-win-big-rigs-over-the-road-racing-gif-15745630")

