"""Package for MHKBZ cog."""
from .unity import Unity

__red_end_user_data_statement__ = (
    "This cog does not persistently store data or metadata about users."
)


async def setup(bot):
    """Load Unity cog."""
    await bot.add_cog(Unity(bot))
